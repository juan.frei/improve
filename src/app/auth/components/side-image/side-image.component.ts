import { Component } from '@angular/core';

@Component({
  selector: 'app-side-image',
  template: `
    <aside class="background-login">
      <figure>
        <img [src]="urlImage" alt="imagen-messi">
      </figure>
    </aside>
  `,
  styleUrls: ['./side-image.component.scss']
})
export class SideImageComponent {
  readonly urlImage: string = './assets/images/messi.jpg';
}
