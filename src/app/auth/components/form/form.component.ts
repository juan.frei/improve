import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NewUser, User } from '../../interfaces/login.interfaces';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  readonly texts = {
    login: {
      title: 'Iniciar sesion',
      buttonName: 'Login',
      register: 'Si no estas registrado podes hacerlo',
      to: '/register'
    },
    register: {
      title: 'Registrar usuario',
      buttonName: 'Registrar',
      register: 'Si ya estas registrado podes ingresar',
      to: '/login'
    }
  }

  @Output() emitToParent = new EventEmitter<User | NewUser>();

  @Input() type: 'login' | 'register' = 'login';
  @Input() loading: boolean = false;

  form: FormGroup = {} as FormGroup;

  get invalidUser() { return this.form.get('user')?.invalid && this.form.get('user')?.touched; }
  get invalidPassword() { return this.form.get('password')?.invalid && this.form.get('password')?.touched; }
  get invalidValidatePassword() { return this.form.get('validatePassword')?.invalid && this.form.get('validatePassword')?.touched; }
  get userWithValue() { return this.form.get('user')?.value; }
  get passwordWithValue() { return this.form.get('password')?.value; }
  get validatePasswordWithValue() { return this.form.get('validatePassword')?.value; }

  constructor(private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.createFrom()
  }

  private createFrom() {
    this.form = this.formBuilder.group({
      user: ['', Validators.required],
      password: ['', Validators.required],
      remind: [false]
    });
    if (this.type === 'register') {
      this.form.addControl('validatePassword', new FormControl('', Validators.required));
    }
  }

  checkItem() {
    const value = this.form.get('remind')?.value;
    this.form.patchValue({ remind: !value });
  }

  buttonAction() {
    this.emitToParent.emit(this.form.value);
  }

  navigateTo() {
    this.router.navigateByUrl(this.texts[this.type].to)
  }

}
