import { Component } from '@angular/core';
import { Router } from '@angular/router';

// Interfaces
import { NewUser } from '../interfaces/login.interfaces';

// Services
import { AudthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../login/login.component.scss']
})
export class RegisterComponent {

  loading: boolean = false;

  constructor(private router: Router, private authService: AudthService) { }

  register(newUser: NewUser) {
    console.log(newUser);
    this.loading = true
    setTimeout(() => {
      this.loading = false;
      this.router.navigateByUrl('/login');
    }, 200000);
  }

}
