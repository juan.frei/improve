import { Component } from '@angular/core';
import { Router } from '@angular/router';

// Interfaces
import { User } from '../interfaces/login.interfaces';

// Services
import { AudthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loading: boolean = false;

  constructor(private router: Router, private authService: AudthService) { }

  login(user: User) {
    this.loading = true
    setTimeout(() => {
      this.loading = false;
      this.router.navigateByUrl('/home');
    }, 2000);
  }

}
