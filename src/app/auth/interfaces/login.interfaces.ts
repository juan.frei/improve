export interface User {
  id?: number;
  user: string;
  password: string;
  record: boolean;
}

export interface NewUser {
  id?: number;
  user: string;
  password: string;
  validatePassword?: string;
  record: boolean;
}
