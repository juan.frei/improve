import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  isLogin: boolean = true;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.checkUrl();
  }

  checkUrl() {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(
      (event: any) => {
        window.scroll(0, 0);
        event.url === '/login' || event.url === '/register' ? this.isLogin = true : this.isLogin = false;
      }
    );
  }

}
