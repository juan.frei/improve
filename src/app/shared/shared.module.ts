import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { ButtonLoadingComponent } from './button-loading/button-loading.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    ButtonLoadingComponent,
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    ButtonLoadingComponent,
  ]
})
export class SharedModule { }
