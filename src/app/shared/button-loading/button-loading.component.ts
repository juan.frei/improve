import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button-loading',
  templateUrl: './button-loading.component.html',
  styleUrls: ['./button-loading.component.scss']
})
export class ButtonLoadingComponent {

  @Output() emit: EventEmitter<void> = new EventEmitter();

  @Input() isValid: boolean = false;
  @Input() loading: boolean = false;
  @Input() buttonText: string = 'Cargar';
  @Input() buttonStyle: string = '';

  emitMethod() {
    this.emit.emit();
  }
}
